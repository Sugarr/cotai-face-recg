# ~/ekyc/face_recognition/backend
import os
import sys
import glob
import time
import datetime
import uuid

from easydict import EasyDict as edict

def process_config(config):
    if not os.path.exists(config.API_DATA_DIR):
        os.makedirs(config.API_DATA_DIR)

    config.DLIB_DETECTOR_MODEL = os.path.abspath(config.DLIB_DETECTOR_MODEL)
    config.DLIB_LANDMARKS_MODEL = os.path.abspath(config.DLIB_LANDMARKS_MODEL)
    config.MTCNN_FACE_DETECTOR_MODEL_DIR = os.path.abspath(config.MTCNN_FACE_DETECTOR_MODEL_DIR)
    config.INSIGHTFACE_DESCRIPTOR_MODEL_DIR = os.path.abspath(config.INSIGHTFACE_DESCRIPTOR_MODEL_DIR)
    config.FAISS_KNN_DATA_DIR = os.path.abspath(config.FAISS_KNN_DATA_DIR)

    if not os.path.exists(config.FAISS_KNN_DATA_DIR):
        os.makedirs(config.FAISS_KNN_DATA_DIR)
    config.FAISS_KNN_INDEX_FILE = os.path.join(config.FAISS_KNN_DATA_DIR, config.FAISS_KNN_INDEX_FILE)
    config.FAISS_KNN_IDX_TO_ID_FILE = os.path.join(config.FAISS_KNN_DATA_DIR, config.FAISS_KNN_IDX_TO_ID_FILE)
    config.FAISS_KNN_ID_TO_IDX_FILE = os.path.join(config.FAISS_KNN_DATA_DIR, config.FAISS_KNN_ID_TO_IDX_FILE)
    config.FAISS_KNN_METADATA_FILE = os.path.join(config.FAISS_KNN_DATA_DIR, config.FAISS_KNN_METADATA_FILE)

### DEFINE CONFIGURATION HERE ---
default = edict()

default.API_HOST = "0.0.0.0"
default.API_PORT = 3333
default.API_DATA_DIR = "api_data"

default.MIN_IMAGE_WIDTH = 640
default.MIN_RELATIVE_PIXEL_DISTANCE_BETWEEN_EYES = 0.0625 # Ideal: 0.125
default.MIN_REGISTRATION_PIXEL_DISTANCE_BETWEEN_EYES = 100 # Ideal: 160
default.MIN_DETECTION_PIXEL_DISTANCE_BETWEEN_EYES = 60 # Ideal: 160
default.MIN_RECOGNITION_PIXEL_DISTANCE_BETWEEN_EYES = 100 # Ideal: 160
default.MIN_LUX_ILLUMINATION = 200
default.MIN_NUMBER_OF_REGISTERED_IMAGE_PER_FACE = 8

default.DLIB_DETECTOR_MODEL = "C:/Users/Sugar/Documents/CODING/GITLAB PROJECT/ekyc/backend/business_layer/models/mmod_human_face_detector.dat"
default.DLIB_LANDMARKS_MODEL = "C:/Users/Sugar/Documents/CODING/GITLAB PROJECT/ekyc/backend/business_layer/models/shape_predictor_68_face_landmarks.dat"
default.DLIB_INPUT_SCALE = 0.25
default.DLIB_FACE_DETECTOR_GPU_IDX = 1

default.MTCNN_FACE_DETECTOR_MODEL_DIR = "third_party_packages/insightface/deploy/mtcnn-model"
default.MTCNN_INPUT_SCALE = 0.25
default.MTCNN_FACE_DETECTOR_GPU_IDX = 1
default.MTCNN_FACE_DETECTOR_THRESHOLD = [0.7, 0.8, 0.95]

default.INSIGHTFACE_FACEFILTER_SIZE_THRESHOLD = 50
default.INSIGHTFACE_FACEFILTER_BLURRINESS_THRESHOLD = 120

default.INSIGHTFACE_INPUT_IMAGESIZE = (112, 112)
default.INSIGHTFACE_DESCRIPTOR_MODEL_DIR = "third_party_packages/insightface/models/model-r50-am-lfw"
default.INSIGHTFACE_DESCRIPTOR_GPU_IDX = 1
default.INSIGHTFACE_DESCRIPTOR_OUTPUT_SIZE = 512

default.FAISS_KNN_DATA_DIR = "data"
default.FAISS_KNN_INDEX_FILE = "face_database.index"
default.FAISS_KNN_IDX_TO_ID_FILE = "idx_to_id.json"
default.FAISS_KNN_ID_TO_IDX_FILE = "id_to_idx.json"
default.FAISS_KNN_METADATA_FILE = "metadata.json"
default.FAISS_KNN_GPU_IDX = 1
default.FAISS_KNN_K = 5 # TEST
default.FAISS_KNN_THRESHOLD = 0.25

process_config(default)