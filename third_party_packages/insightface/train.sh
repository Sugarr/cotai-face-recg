# ~/insightface
cd recognition
export MXNET_CPU_WORKER_NTHREADS=24
export MXNET_ENGINE_TYPE=ThreadedEnginePerDevice

cp sample_config.py config.py

# # (1). Train ArcFace with LResNet100E-IR.
# CUDA_VISIBLE_DEVICES='0,1,2,3' python -u train.py --network r100 --loss arcface --dataset emore --pretrained ../models/

# (2). Train CosineFace with LResNet50E-IR.
# CUDA_VISIBLE_DEVICES='0,1,2,3' python -u train.py --network r50 --loss cosface --dataset emore\
#                                                   --lr 0.1 --per-batch-size 32

CUDA_VISIBLE_DEVICES='0,1,2,3' python -u train.py --network r50 --loss arcface --dataset emore\
                                                  --lr 0.0 --per-batch-size 32\
                                                  --pretrained ../models/model-r50-am-lfw/model

# # (3). Train Softmax with LMobileNet-GAP.
# CUDA_VISIBLE_DEVICES='0,1,2,3' python -u train.py --network m1 --loss softmax --dataset emore --pretrained ../models/

# # (4). Fine-turn the above Softmax model with Triplet loss.
# CUDA_VISIBLE_DEVICES='0,1,2,3' python -u train.py --network m1 --loss triplet --lr 0.005 --pretrained ./models/m1-softmax-emore,1