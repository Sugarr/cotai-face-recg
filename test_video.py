import os
import glob
import sys
import random
import time
import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
from PIL import Image
from sklearn.neighbors import BallTree
from business_layer.core.insightface import (
    InsightFaceMTCNNDetector,
    InsightFacePreprocessor,
    InsightFaceDescriptor,
    KNNFaceSearcher
)
import natsort

from business_layer.core.dlibfaces import DlibDetector
from business_layer.core.pipeline import (
    InputImageFilter,
    FaceDetector,
    InputFaceFilter,
    FacePreprocessor,
    FaceDescriptor,
    FaceSearcher
)
from config import default
import argparse

def ids_to_label(ids,dist, label):
    ls = []
    for i in range(len(ids)):
        predict = ids[i]
        distance = dist[i]
        if predict.size == 0:
            ls.append(-1)
        else:
            
            score_class = np.zeros(max(label)+1)
            for j in range(len(predict)):
                score_class[label[predict[j]]] += 1/distance[j]
            ls.append(np.argmax(score_class))
    return ls


def resize(image, newsize):
    """Resize `image` to `newsize` while keeping proportions
    
    Arguments:
        `image`: ndarray of shape (h, w, c)
            BGR image
        `newsize`: tuple of 2 ints
            Width and height
    Returns:
        `resized_image`: ndarray of shape (nh, nw, c)
            Resized BGR image
    """
    maxscale = max(newsize[0]/image.shape[1], newsize[1]/image.shape[0])
    padded_image_size = (int(maxscale/newsize[0]*image.shape[1]*image.shape[1]), 
                         int(maxscale/newsize[1]*image.shape[0]*image.shape[0]))
    padtop = max((padded_image_size[1] - image.shape[0])//2, 0)
    padbottom = max((padded_image_size[1] - image.shape[0]) - padtop, 0)
    padleft = max((padded_image_size[0] - image.shape[1])//2, 0)
    padright = max((padded_image_size[0] - image.shape[1]) - padleft, 0)
    image = np.pad(image, ((padleft, padright), (padtop, padbottom), (0, 0)), mode='constant')
    image = cv2.resize(image, newsize)
    return image

def visualize_detection_results(image, bboxes, landmarks, face_validities):
    for i, (bbox, landmark) in enumerate(zip(bboxes, landmarks)):
        bbox = bbox.astype(int)
        landmark = landmark.astype(int)
        
        if face_validities[i]:
            color = (0, 255, 0)
        else:
            color = (0, 0, 255)
        
        cv2.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]), color, 5)
        for point in landmark:
            cv2.circle(image, (point[0], point[1]), 3, color, 2)
    return image

print("Loading Embedding and Tree ...")
embedding = np.load('./emb_and_tree/x_train_5.npy')
label = np.load('./emb_and_tree/y_label_train_5.npy')
tree = BallTree(embedding)


print("Loading config from config.py ...")


input_image_filter = InputImageFilter(default.MIN_IMAGE_WIDTH,
                                      default.MIN_LUX_ILLUMINATION,
                                      default.MIN_NUMBER_OF_REGISTERED_IMAGE_PER_FACE)


detector = DlibDetector(default.DLIB_DETECTOR_MODEL, 
                        default.DLIB_LANDMARKS_MODEL, 
                        default.DLIB_INPUT_SCALE,
                        default.DLIB_FACE_DETECTOR_GPU_IDX)
detector = FaceDetector(detector)

input_face_filter = InputFaceFilter(default.MIN_RELATIVE_PIXEL_DISTANCE_BETWEEN_EYES,
                                    default.MIN_REGISTRATION_PIXEL_DISTANCE_BETWEEN_EYES,
                                    default.MIN_DETECTION_PIXEL_DISTANCE_BETWEEN_EYES,
                                    default.MIN_RECOGNITION_PIXEL_DISTANCE_BETWEEN_EYES)

preprocessor = InsightFacePreprocessor(default.INSIGHTFACE_INPUT_IMAGESIZE)
preprocessor = FacePreprocessor(preprocessor)

descriptor = InsightFaceDescriptor(default.INSIGHTFACE_DESCRIPTOR_MODEL_DIR,
                                   None,
                                   default.INSIGHTFACE_INPUT_IMAGESIZE)

descriptor = FaceDescriptor(descriptor)

# searcher = KNNFaceSearcher(default.FAISS_KNN_INDEX_FILE,
#                            default.FAISS_KNN_IDX_TO_ID_FILE,
#                            default.FAISS_KNN_ID_TO_IDX_FILE,
#                            default.FAISS_KNN_METADATA_FILE,
#                            default.INSIGHTFACE_DESCRIPTOR_OUTPUT_SIZE,
#                            default.FAISS_KNN_K,
#                            default.FAISS_KNN_THRESHOLD,
#                            None)
# searcher = FaceSearcher(searcher)


def capture_and_predict(true_label, path, show = None):
    video_path = './video/'+str(path)
    cap = cv2.VideoCapture(video_path)

    runtime = {"readtime": [],
            "preprocesstime": [],
            "inputfiltertime": [], 
            "detecttime": [],
            "facefiltertime": [],
            "facepreprocesstime": [],
            "describetime": []}

    start = time.time()
    count = 0
    
    true_predict = 0
    false_predict = 0
    
    while True:        
        start = time.time()
        ret, frame = cap.read()
        runtime["readtime"].append(time.time() - start)
        if ret == False:
            print(true_label, video_path)
            print(ret)
            break
        start = time.time()
        # Resize image và tạo list chứa dictionary
        frame = resize(frame, (1280, 720))
        items = [{"image": frame, "task": "detection"}]
        runtime["preprocesstime"].append(time.time() - start)
        
        # Kiểm tra input có phù hợp yêu cầu không (Yêu cầu có để cập trong file insigtface.py)
        start = time.time()
        items = input_image_filter.filter(items)
        runtime["inputfiltertime"].append(time.time() - start)
        # Chỉ tiếp tục cắt khuôn mặt khi tấm hình phù hợp yêu cầu

        # Nhận diện khuông mặt trong tấm hình input
        start = time.time()
        items = detector.detect(items)
        runtime["detecttime"].append(time.time() - start)
        
        if items[0]["validity"] == True:
            

            # Kiểm tra bboxes và landmarks
            start = time.time()
            items = input_face_filter.filter(items)
            runtime["facefiltertime"].append(time.time() - start)

            # Xử lý khuôn mặt
            start = time.time()
            items = preprocessor.preprocess(items)
            runtime["facepreprocesstime"].append(time.time() - start)

            # Lấy embedding của khuôn mặt sử dụng model Arcface 
            start = time.time()
            items = descriptor.describe(items)
            runtime["describetime"].append(time.time() - start)
            start = time.time()

            # Sử dụng embedding để dự đoán tên
            ids, dist = tree.query_radius(items[0]['embeddings'],  r = 0.32, return_distance = True)

            y_predict = ids_to_label(np.array(ids), np.array(dist), label)
            frame = visualize_detection_results(items[0]["image"], items[0]["bboxes"], items[0]["landmarks"],
                                    items[0]["face_validities"])
            count += 1

            if y_predict[0] != -1:
                if y_predict[0] == true_label:
                    true_predict += 1
                else:
                    print(y_predict[0])
                    false_predict += 1
        if show == True:
            cv2.putText(frame, str(time.time() - start), (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 1, (30, 255, 30), 2, cv2.LINE_AA)
            cv2.putText(frame, 'True label:' +str(true_label), (20, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (30, 255, 30), 2, cv2.LINE_AA)
            cv2.putText(frame, 'Predict:' +str(y_predict[0]), (20, 80), cv2.FONT_HERSHEY_SIMPLEX, 1, (30, 255, 30), 2, cv2.LINE_AA)
            cv2.putText(frame, 'Press S to skip video', (20, 110), cv2.FONT_HERSHEY_SIMPLEX, 1, (30, 255, 30), 2, cv2.LINE_AA)

            cv2.imshow('Frame', frame)
            if cv2.waitKey(1) & 0xFF == ord('s'):
                break

            if cv2.waitKey(1) & 0xFF == ord('q'):
                raise('STOP')
    return true_predict, false_predict




if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--show', default = None)
    
    args = parser.parse_args()
    video_path = sorted(os.listdir('./video'))
    video_path = natsort.natsorted(video_path)
    result = {'True label':[], 'False predict':[] , 'True predict':[]}
    for path in video_path:
        true_label = path.split('_')[0]
        true_predict, false_predict = capture_and_predict(int(true_label),path, args.show)
        result['True label'].append(true_label)
        result['False predict'].append(false_predict)
        result['True predict'].append(true_predict)
        wirte_result = pd.DataFrame(result)
        wirte_result.to_csv('./result.csv', index=False)

# print(f"Average runtime: {(time.time() - start)/int(cap.get(cv2.CAP_PROP_FRAME_COUNT))}")
# runtime = pd.DataFrame(runtime)
# runtime["total"] = runtime.sum(axis=1)
# runtime["totalprocesstime"] = runtime.drop(["readtime", "total"], axis=1).sum(axis=1)

# runtime.to_csv("cpu_runtime.csv", index=None)