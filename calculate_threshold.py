import numpy as np
import time
import cv2
import pickle
from sklearn.neighbors import BallTree
import json
from datetime import datetime 
from sklearn import metrics

def confusion_matrix_analysis(confusion_matrix):
    for i in range(len(confusion_matrix)):
        print('\nClass %s: Dự đoán đúng %s'%(i, np.max(confusion_matrix[i])))

        if len(np.unique(confusion_matrix[i])) != 2:
            confusion_matrix[i][np.argmax(confusion_matrix[i])] = 0
            print('\t  Dự đoán sai %s'%(sum(confusion_matrix[i])))
            wrong_class = confusion_matrix[i] > 0
            for j in range(len(wrong_class)):
                if wrong_class[j] == True:
                    print('\t  Class bị nhận nhầm: %s số lượng %s'%(j, confusion_matrix[i,j]))


def ids_to_label(ids,dist, label):
    ls = []
    for i in range(len(ids)):
        predict = ids[i]
        distance = dist[i]
        if predict.size == 0:
            ls.append(-1)
        else:
            
            score_class = np.zeros(max(label)+1)
            for j in range(len(predict)):
                score_class[label[predict[j]]] += 1/distance[j]
            ls.append(np.argmax(score_class))
    return ls


print('\nLoad data')
emb = np.load('./emb_and_tree/X_train_10.npy')
label = np.load('./emb_and_tree/y_label_train_10.npy')

x_test  = np.load('./emb_and_tree/X_test_10.npy')
y_test = np.load('./emb_and_tree/y_label_test_10.npy')

print('total train:',label.shape[0])
print('total test:',y_test.shape[0])
# Fit embeding to KDtree
print('\nFit embedding to BallTree')
t0 = time.time()
tree = BallTree(emb)
print('\nDone:',time.time()-t0)
true_distance = []
false_distance = []

print('\nStart query')
t0 = time.time()
ids, dist = tree.query_radius(x_test, r = 0.20, return_distance = True)
print('\nDone Total time:',time.time()-t0)
print('Time per 1 point:',(time.time()-t0)/len(y_test))

# for i, point in enumerate(ids):
#     for j, index in enumerate(point):
#         if y_test[i] == label[index]:
#             true_distance.append(dist[i,j])
#         else:
#             false_distance.append(dist[i,j])

# print(np.min(true_distance))
# print(np.min(false_distance))

# print(np.max(true_distance))
# print(np.max(false_distance))

# print(np.mean(true_distance))
# print(np.mean(false_distance))

# count_false = 0
# count_true = 0
# thres_hold = 0.32
# for dis in false_distance:
#     if dis <= thres_hold:
#         count_false+=1

# for dis in true_distance:
#     if dis <= thres_hold:
#         count_true+=1
# print('false:',count_false)
# print('True:',count_true)

# print('True shape:', len(true_distance))
# print('false shape:', len(false_distance))

y_predict = ids_to_label(np.array(ids), np.array(dist), label)

BallTree_confusion = metrics.confusion_matrix(y_test, y_predict)
print('\n',confusion_matrix_analysis(BallTree_confusion))

print('\n',metrics.classification_report(y_test, y_predict))