from multiprocessing import Process, Queue, Lock, Value
import multiprocessing as mp
from threading import Thread
from queue import *

class CustomizedQueue():
    def __init__(self, name, maxsize=30):
        self.queue = Queue()
        self.name = name
        self.maxsize = maxsize
        self.get_lock = Lock()
        self.put_lock = Lock()

    def qput(self, item):
        self.put_lock.acquire()
        try:
            while self.queue.qsize() >= self.maxsize:
                item = self.queue.get()
            self.queue.put(item)
        except:
            pass
        self.put_lock.release()

    def qget(self, timeout=None):
        self.get_lock.acquire()
        try:
            item = self.queue.get(timeout=timeout)
            while self.queue.qsize() > self.maxsize:
                item = self.queue.get()
        except:
            item = None
        self.get_lock.release()
        return item

class Worker(Process):
    def __init__(self, wait_for_init, iqueue=None, oqueues=None, processor=None):
        super(Worker, self).__init__()
        self.iqueue = iqueue
        self.oqueues = oqueues
        self.processor = processor
        self.deamon = True
        self.wait_for_init = wait_for_init
    
    def run(self):
        self.processor.init()
        self.wait_for_other_process_init()

        oqueue_idx = 0
        while True:
            item = self.iqueue.qget(2) if self.iqueue is not None else -1
            if item is not None:
                item = self.processor.runOn(item)
                if self.oqueues is not None:
                    self.oqueues[oqueue_idx].qput(item)
                    oqueue_idx = (oqueue_idx+1) % len(self.oqueues)
                if self.processor.stop:
                    break
            else:  # No more item after 5 seconds
                break
        print(self.processor.name, "Done!")

    def wait_for_other_process_init(self):
        print(self.processor.name, "Done init!")
        self.wait_for_init.value -= 1
        while self.wait_for_init.value > 0:
            time.sleep(0.5)
        print(self.processor.name, self.wait_for_init.value, "Done waiting!")

if __name__ == "__main__":
    mp.set_start_method('spawn')
    n_locator = 8
    frameq = [CustomizedQueue("frameq "+str(i)) for i in range(n_locator)]
    faceq = CustomizedQueue("faceq")

    num_init = Value('i', n_locator + 2)
    video_capture = Worker(oqueues=frameq, processor=VideoCaptureProcessor(
        path="/home/tqlong/Downloads/video.mp4"), wait_for_init=num_init)
    face_locators = [Worker(iqueue=frameq[i], oqueues=[faceq],
                            processor=FaceLocator(i), wait_for_init=num_init) for i in range(n_locator)]
    video_render = Worker(iqueue=faceq, oqueues=None,
                          processor=VideoRenderProcessor(), wait_for_init=num_init)

    processes = [video_capture, video_render] + face_locators
    [t.start() for t in processes]
    [t.join() for t in processes]