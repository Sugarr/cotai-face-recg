import binascii
import numpy as np
import cv2

def bytes2image(binary_image):
    """Convert a base64 string (utf-8 encoded) to a CV2 image
    
    Arguments:
        `binary_image`: str
            The utf-8 encoded base64 string
    Returns:
        `image`: ndarray of shape (h, w, c)
            The output image.
    """
    image_content = binascii.a2b_base64(binary_image)
    image = cv2.imdecode(np.frombuffer(image_content, np.uint8), -1)