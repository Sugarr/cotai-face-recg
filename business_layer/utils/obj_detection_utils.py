import os, sys, glob
import time
import math
import cv2
import numpy as np
from itertools import repeat
try:
    from itertools import izip
except ImportError:
    izip = zip

def nms(boxes, overlap_threshold, mode='Union'):
    """non max suppression
    
    Arguments:
        `box`: numpy array n x 5
            input bbox array
        `overlap_threshold`: float number
            threshold of overlap
        `mode`: string
            how to compute overlap ratio, 'Union' or 'Min'
    Returns:
        `pick`: index list of the selected bbox
    """
    if len(boxes) == 0:
        return []

    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")
    
    pick = []
    x1, y1, x2, y2, score = [boxes[:, i] for i in range(5)]
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(score)

    while len(idxs) > 0:
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        inter = w * h
        if mode == 'Min':
            overlap = inter / np.minimum(area[i], area[idxs[:last]])
        else:
            overlap = inter / (area[i] + area[idxs[:last]] - inter)

        idxs = np.delete(
            idxs, 
            np.concatenate(([last], np.where(overlap > overlap_threshold)[0]))
        )
    return pick

if __name__ == "__main__":
    boxes = list(np.random.rand(10, 20, 5))
    start = time.time()
    for i in range(1000):
        batch_nms(boxes, 0.5)
    print(time.time() - start)
    start = time.time()
    for i in range(1000):
        for j in range(len(boxes)):
            nms(boxes[j], 0.5)
    print(time.time() - start)