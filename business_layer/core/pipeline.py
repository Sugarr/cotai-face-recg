import os, glob, sys
import numpy as np
import cv2

import config


class InputImageFilter:
    def __init__(self, min_image_width=640,
                        min_lux_illumination=200,
                        min_number_of_registered_image_per_face=8):
        self.min_image_width = min_image_width
        self.min_lux_illumination = min_lux_illumination
        self.min_number_of_registered_image_per_face = min_number_of_registered_image_per_face
    
    def filter(self, items):
        r"""
        Arguments:
            `items`: list of Python dicts 
                Each dict has a field "image", which is a ndarray of shape (h, w, c) representing a RGB images
        Returns:
            `items`: list of Python dicts
                Original dicts with additional field "validity"
        """
        for item in items:
            item["validity"] = item.get("validity", True)
            item["validity"] = item["validity"] and self.__check_image_width(item)
        return items
    
    def __check_image_width(self, item):
        if item["image"].shape[1] < self.min_image_width:
            return False
        return True


class FaceDetector:
    def __init__(self, detector):
        r"""
        Arguments:
            `detector`: an instance of `business_layer.core.interfaces.IFaceDetector`
        """
        self.detector = detector
    
    def detect(self, items):
        r"""
        Arguments:
            `items`: list of Python dicts 
                Each dict has a field "image", which is a ndarray of shape (h, w, c) representing a RGB images
                Each dict has a field "validity", which indicates whether the image is valid or not 
        Returns:
            `items`: list of Python dicts
                Original dicts with additional fields "bboxes" and "landmarks"
        """
        for item in items:
            if not item["validity"]:
                continue
            item["bboxes"], item["landmarks"] = self.detector.detect(item["image"])
            item["validity"] = item["validity"] and self.__check_no_face(item)
        return items
    
    def __check_no_face(self, item):
        if len(item["bboxes"]) == 0:
            return False
        return True


class InputFaceFilter:
    def __init__(self, min_relative_pixel_distance_between_eyes = 0.125,
                       min_registration_pixel_distance_between_eyes=100,
                       min_detection_pixel_distance_between_eyes=60,
                       min_recognition_pixel_distance_between_eyes=100):
        self.min_relative_pixel_distance_between_eyes = min_relative_pixel_distance_between_eyes
        self.min_pixel_distance_between_eyes = {
            "registration": min_recognition_pixel_distance_between_eyes,
            "detection": min_detection_pixel_distance_between_eyes,
            "recognition": min_recognition_pixel_distance_between_eyes
        }
    
    def filter(self, items):
        r"""
        Arguments:
            `items`: list of Python dicts 
                Each dict has a field "landmarks", which is a ndarray of shape (n_bboxes, n_landmarks, 2) representing absolute (x, y) coordinates of face landmarks of faces in the image
                Each dict has a field "validity", which indicates whether the image is valid or not 
                Each dict has a field "task", which indicates which task the image will be used
        Returns:
            `items`: list of Python dicts
                Original dicts with additional fields "face_validities", which is a ndarray of shape (n_bboxes)
        """
        for item in items:
            if not item["validity"]:
                continue
            elif item["landmarks"].shape[1] == 5:
                item["face_validities"] = self.__check_5_landmarks(item)
            else:
                item["face_validities"] = self.__check_68_landmarks(item)
        return items
    
    def __check_5_landmarks(self, item):
        squared_pixel_distance_between_eyes = np.sum((item["landmarks"][:, 0, :] - item["landmarks"][:, 1, :])**2, axis=-1)
        face_validities = []
        for squared_distance in squared_pixel_distance_between_eyes:
            validity = (squared_distance >= int(self.min_relative_pixel_distance_between_eyes * item["image"].shape[1])**2)
            validity = validity and (squared_distance >= self.min_pixel_distance_between_eyes[item["task"]]**2)
            face_validities.append(validity)
        face_validities = np.array(face_validities)
        return face_validities

    def __check_68_landmarks(self, item):
        return True


class FacePreprocessor:
    def __init__(self, preprocessor):
        r"""
        Arguments:
            `preprocessor`: an instance of `business_layer.core.interfaces.IFacePreprocessor`
        """
        self.preprocessor = preprocessor
    
    def preprocess(self, items):
        r"""
        Arguments:
            `items`: list of Python dicts
                Each dict has a field "bboxes", which is a ndarray of shape (n_bboxes, 5) representing absolute (x1, y1, x2, y2, score) of each face in the image
                Each dict has a field "landmarks", which is a ndarray of shape (n_bboxes, n_landmarks, 2) representing absolute (x, y) coordinates of face landmarks of faces in the image
                Each dict has a field "validity", which indicates whether the image is valid or not
        Returns:
            `items`: list of Python dicts
                Original dicts with additional fields "faces", which is a ndarray of shape (n_faces, h, w, c) representing cropped faces from the image
        """
        for item in items:
            if not item["validity"]:
                continue
            item["faces"] = self.preprocessor.process(item["image"], item["bboxes"], item["landmarks"])
        return items


class FaceDescriptor:
    def __init__(self, descriptor):
        r"""
        Arguments:
            `descriptor`: an instance of `business_layer.core.interfaces.IFaceDescriptor`
        """
        self.descriptor = descriptor
    
    def describe(self, items):
        r"""
        Arguments:
            `items`: list of Python dicts
                Each dict has a field "faces", which is a ndarray of shape (n_faces, h, w, c) representing cropped faces from the image
        Returns:
            `items`: list of Python dicts
                Original dicts with additional fields "embeddings", which is a ndarray of shape (n_faces, n_features)
        """
        for item in items:
            if not item["validity"]:
                continue
            item["embeddings"] = self.descriptor.process(item["faces"])
        return items


class FaceSearcher:
    def __init__(self, searcher):
        r"""
        Arguments:
            `descriptor`: an instance of `business_layer.core.interfaces.IFaceSearcher`
        """
        self.searcher = searcher
    
    def search(self, items):
        r"""
        Arguments:
            `items`: list of Python dicts
                Each dict has a field "embeddings", which is a ndarray of shape (n_faces, n_features)
        Returns:
            `items`: list of Python dicts
                Original dicts with additional fields "ids", which is a list of str representing face IDs
        """
        for item in items:
            if not item["validity"]:
                continue
            item["ids"] = self.searcher.predict(item["embeddings"])
        return items
    
    def append(self, items):
        r"""Note: choose biggest face from all valid faces to append
        
        Arguments:
            `items`: list of Python dicts
                Each dict has a field "embeddings", which is a ndarray of shape (n_faces, n_features), and a field "id", which is the ID to insert to database
            Returns:
                `items`: list of Python dicts
                    Original dicts with additional fields "inserted_bbox", the coordinates of inserted bounding box, this field is None if no face is inserted
        """
        for item in items:
            if not item["validity"]:
                continue
            valid_bboxes = item["bboxes"][item["face_validities"]]
            valid_embeddings = item["embeddings"][item["face_validities"]]
            if len(valid_bboxes) == 0:
                item["inserted_bbox"] = None
                continue
            chosen_embedding= self.__get_largest_box(valid_bboxes)
            item["inserted_bbox"] = valid_bboxes[chosen_embedding]
            chosen_embedding = valid_embeddings[chosen_embedding][None, ...]
            self.searcher.insert_to_database(chosen_embedding, item["id"])
        return items
    
    def blind_append(self, items):
        """Append to database without checking for validity
        """
        for item in items:
            self.searcher.insert_to_database(item["embeddings"], item["id"])
    
    def __get_largest_box(self, bboxes):
        areas = (bboxes[:, 2] - bboxes[:, 0])*(bboxes[:, 3] - bboxes[:, 1])
        return np.argmax(areas)
        
    def save(self):
        self.searcher.save_database()

if __name__ == "__main__":
    input_image_filter = InputImageFilter(config.MIN_IMAGE_WIDTH,
                                          config.MIN_LUX_ILLUMINATION,
                                          config.MIN_NUMBER_OF_REGISTERED_IMAGE_PER_FACE)