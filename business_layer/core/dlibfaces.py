import numpy as np
import cv2
import dlib
from imutils import face_utils

import business_layer.core.interfaces as interfaces
import config

class DlibDetector(interfaces.IFaceDetector):
    def __init__(self, detector_model, landmarks_model, inputscale, gpu=None):
        dlib.cuda.set_device(0)
        self.detector = dlib.cnn_face_detection_model_v1(detector_model)
        self.predictor = dlib.shape_predictor(landmarks_model)
        self.inputscale = inputscale
    
    def detect(self, image):
        faces = self.__scale_and_detect(image)
        if len(faces) == 0:
            return np.array([]), np.array([])
        bboxes, landmarks = [], []
        for face in faces:
            bbox = self.__rescale_rect(face.rect)
            landmark = self.__get_landmarks(image, bbox)
            bboxes.append([bbox.left(), bbox.top(), bbox.right(), bbox.bottom(), face.confidence])
            landmarks.append(landmark)
        bboxes, landmarks = self.__postprocess(bboxes, landmarks, image)
        return bboxes, landmarks

    def __scale_and_detect(self, image):
        newsize = int(image.shape[1] * self.inputscale), int(image.shape[0] * self.inputscale)
        image = cv2.resize(image, newsize)
        faces = self.detector(image[..., ::-1], 0)
        return faces

    def __rescale_rect(self, rect):
        rect = dlib.rectangle(int(rect.left() / self.inputscale), 
                              int(rect.top() / self.inputscale), 
                              int(rect.right() / self.inputscale), 
                              int(rect.bottom() / self.inputscale))
        return rect
    
    def __get_landmarks(self, image, rect):
        landmarks_68 = self.predictor(image, rect)
        landmarks_68 = face_utils.shape_to_np(landmarks_68)
        landmarks_5 = np.array([np.mean(landmarks_68[36:42], axis=0),
                                np.mean(landmarks_68[42:48], axis=0),
                                landmarks_68[30], landmarks_68[48], landmarks_68[54]])
            
        return landmarks_5
    
    def __postprocess(self, bboxes, landmarks, image):
        bboxes, landmarks = np.array(bboxes), np.array(landmarks)
        bboxes[:, [0, 2]] = np.clip(bboxes[:, [0, 2]], 0, image.shape[1])
        bboxes[:, [1, 3]] = np.clip(bboxes[:, [1, 3]], 0, image.shape[0])
        return bboxes, landmarks