import argparse
from argparse import Namespace
import cv2
import os
import sys
import time
import json
import numpy as np
import mxnet as mx
import sklearn
import skimage
# import faiss

working_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.join(working_dir, '..', '..', 'third_party_packages', 'insightface', 'deploy'))
import face_model
from mtcnn_detector import MtcnnDetector

import business_layer.core.interfaces as interfaces
import config

class InsightFaceMTCNNDetector(interfaces.IFaceDetector):
    def __init__(self, model_dir, inputscale, gpu=0, det_threshold=[0.6,0.7,0.8]):
        """Detector from InsightFace
        
        Arguments:
            `gpu`: int
                GPU index
            `det_threshold`: list of 3 floats
                Threshold for 3 stages of MTCNN
        """
        super().__init__()
        if gpu is None:
            ctx = mx.gpu(0)
        else:
            ctx = mx.gpu(0)
        mtcnn_path = os.path.join(model_dir)
        detector = MtcnnDetector(model_folder=mtcnn_path, ctx=ctx, num_worker=1, accurate_landmark=True, threshold=det_threshold)
        self.model = detector
        self.inputscale = inputscale
    
    def detect(self, image):
        h, w, _ = image.shape
        ret = self.__scale_and_detect(image)
        if ret is None:
            return np.array([]), np.array([])
        bboxes, landmarks = ret
        bboxes = self.__postprocess_bboxes(bboxes, h, w)
        landmarks = self.__postprocess_landmarks(landmarks, h, w)
        return bboxes, landmarks
    
    def __scale_and_detect(self, image):
        newsize = int(image.shape[1] * self.inputscale), int(image.shape[0] * self.inputscale)
        image = cv2.resize(image, newsize)
        ret = self.model.detect_face(image, det_type=0)
        return ret
    
    def __postprocess_bboxes(self, bboxes, h, w):
        bboxes[:, [0, 2]] = np.clip(bboxes[:, [0, 2]] / self.inputscale, 0, w)
        bboxes[:, [1, 3]] = np.clip(bboxes[:, [1, 3]] / self.inputscale, 0, h)
        return bboxes
    
    def __postprocess_landmarks(self, landmarks, h, w):
        landmarks = landmarks.reshape([-1, 5, 2], order="F")
        landmarks[..., 0] = np.clip(landmarks[..., 0] / self.inputscale, 0, w)
        landmarks[..., 1] = np.clip(landmarks[..., 1] / self.inputscale, 0, h)
        return landmarks


class InsightFacePreprocessor(interfaces.IFacePreprocessor):
    def __init__(self, image_size=(112, 112)):
        super().__init__()

        self.image_size = image_size
        self.desired_landmarks = np.array([
            [38.2946, 51.6963],
            [73.5318, 51.5014],
            [56.0252, 71.7366],
            [41.5493, 92.3655],
            [70.7299, 92.2041] ], dtype=np.float32)
        self.tform = skimage.transform.SimilarityTransform()
    
    def process(self, image, bboxes, landmarks):
        cropped_faces = []
        for i in range(len(bboxes)):
            nimg = self.__process_single_box(image, landmarks[i])
            cropped_faces.append(nimg[None, :, :, :])
        cropped_faces = np.concatenate(cropped_faces, axis=0)
        return cropped_faces
    
    def __process_single_box(self, image, landmarks):
        """Preprocess and align images
        """
        self.tform.estimate(landmarks, self.desired_landmarks)
        M = self.tform.params[:2,:]
        warped = cv2.warpAffine(image, M, (self.image_size[1], self.image_size[0]), borderValue=0.0)
        return warped

class InsightFaceFilter(interfaces.IFaceFilter):
    def __init__(self, size_threshold=50, blurriness_threshold=120):
        """
        
        Arguments:
            `size_threshold`: int
                Lower bound for face size. If one of the face size is below this bound, the face is removed 
            `blurriness_threshold`: float
                Lower bound for face blurriness. If the face's blurriness is below this bound, the face is removed
        """
        super().__init__()
        self.size_threshold = size_threshold
        self.blurriness_threshold = blurriness_threshold
    
    def process(self, cropped_faces):
        kept_idx = []
        for i, face in enumerate(cropped_faces):
            is_kept = self.__examine_single_face(face)
            if is_kept:
                kept_idx.append(i)
        filtered_cropped_faces = cropped_faces[kept_idx]
        return filtered_cropped_faces
    
    def __examine_single_face(self, face):
        return True

class InsightFaceDescriptor(interfaces.IFaceDescriptor):
    def __init__(self, model_dir, gpu=None, image_size=(112, 112)):
        super().__init__()
        if gpu is None:
            ctx = mx.gpu(0)
        else:
            ctx = mx.gpu(0)
        model = os.path.join(model_dir, "model")+",0"
        self.model = self.__get_model(ctx, image_size, model, "fc1")
    
    def __get_model(self, ctx, image_size, model_str, layer):
        _vec = model_str.split(',')
        assert len(_vec)==2
        prefix = _vec[0]
        epoch = int(_vec[1])
        print('loading',prefix, epoch)
        sym, arg_params, aux_params = mx.model.load_checkpoint(prefix, epoch)
        all_layers = sym.get_internals()
        sym = all_layers[layer+'_output']
        model = mx.mod.Module(symbol=sym, context=ctx, label_names = None)
        model.bind(data_shapes=[('data', (1, 3, image_size[0], image_size[1]))])
        model.set_params(arg_params, aux_params)
        return model
    
    def process(self, cropped_faces):
        cropped_faces = cropped_faces[..., ::-1].transpose((0, 3, 1, 2))
        data = mx.nd.array(cropped_faces)
        db = mx.io.DataBatch(data=(data, ))
        self.model.forward(db, is_train=False)
        face_embeddings = self.model.get_outputs()[0].asnumpy()
        face_embeddings = sklearn.preprocessing.normalize(face_embeddings)
        return face_embeddings

class KNNFaceSearcher(interfaces.IFaceSearcher):
    def __init__(self, index_file=None, idx_to_id_file=None, id_to_idx_file=None, metadata_file=None, n_features=128, k=5, threshold=0.25, gpu=None):
        super().__init__()
        
        self.index_file = index_file
        self.index = self.__load_index(n_features, gpu)
        
        self.idx_to_id_file = idx_to_id_file
        self.id_to_idx_file = id_to_idx_file
        self.idx_to_id, self.id_to_idx = self.__load_labels()
        
        self.metadata_file = metadata_file
        self.metadata = self.__load_metadata()
        
        self.k = k
        self.threshold = threshold
        self.label_encoder = sklearn.preprocessing.OneHotEncoder()
    
    def __load_index(self, n_features, gpu):
        if not os.path.exists(self.index_file):
            index = faiss.IndexFlatIP(n_features)
            index = faiss.IndexIDMap(index)
            faiss.write_index(index, self.index_file)
        else:
            index = faiss.read_index(self.index_file)
        return index
    
    def __load_labels(self):
        if not os.path.exists(self.idx_to_id_file):
            idx_to_id = {"-1": "unknown"}
            with open(self.idx_to_id_file, "w") as f:
                json.dump(idx_to_id, f)
        else:
            with open(self.idx_to_id_file) as f:
                idx_to_id = json.load(f)
        
        if not os.path.exists(self.id_to_idx_file):
            id_to_idx = {"unknown": "-1"}
            with open(self.id_to_idx_file, "w") as f:
                json.dump(id_to_idx, f)
        else:
            with open(self.id_to_idx_file) as f:
                id_to_idx = json.load(f)

        return idx_to_id, id_to_idx
    
    def __load_metadata(self):
        if not os.path.exists(self.metadata_file):
            metadata = {
                "n_people": 0
            }
            with open(self.metadata_file, "w") as f:
                json.dump(metadata, f)
        else:
            with open(self.metadata_file) as f:
                metadata = json.load(f)
        return metadata
    
    def insert_to_database(self, face_embeddings, face_id):
        """
        Arguments:
            `face_embeddings`: ndarray of shape (n_faces, n_features)
                Face embeddings of detected faces
            `face_id`: str
                ID of the faces to be inserted
        """
        if face_id in self.id_to_idx:
            face_labels = np.repeat(int(self.id_to_idx[face_id]), len(face_embeddings))
        else:
            face_labels = np.repeat(self.metadata["n_people"], len(face_embeddings))
            self.idx_to_id[str(self.metadata["n_people"])] = face_id
            self.id_to_idx[face_id] = str(self.metadata["n_people"])
            self.metadata["n_people"] += 1
        self.index.add_with_ids(face_embeddings, face_labels)
    
    def save_database(self):
        faiss.write_index(self.index, self.index_file)
        with open(self.idx_to_id_file, "w") as f:
            json.dump(self.idx_to_id, f)
        with open(self.id_to_idx_file, "w") as f:
            json.dump(self.id_to_idx, f)
        with open(self.metadata_file, "w") as f:
            json.dump(self.metadata, f)
    
    def predict(self, face_embeddings):
        """
        Arguments:
            `face_embeddings`: ndarray of shape (n_faces, n_features)
                Face embeddings of detected faces
        """
        similarity, labels = self.index.search(face_embeddings, self.k)
        preds = []
        for i in range(len(similarity)):
            preds.append(self.__major_vote(labels[i], similarity[i]))
        return preds
    
    def __major_vote(self, labels, similarity):
        labels = labels[similarity >= self.threshold]
        if len(labels) == 0:
            return "unknown"
        labels, counts = np.unique(labels, return_counts=True)
        face_id = self.idx_to_id[str(labels[np.argmax(counts)])]
        return face_id