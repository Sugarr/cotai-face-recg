class IFaceDetector:
    def __init__(self):
        pass

    def detect(self, image):
        """Detect faces and corresponding landmarks
        
        Arguments:
            `image`: ndarray of shape (h, w, c)
                A BGR image
        Returns:
            `bboxes`: ndarray of shape (n_bboxes, 5)
                Each bbox is of the form (x1, y1, x2, y2, score).
                The coordinates are floating point absolute coordinates w.r.t the image
                In case of no face detected, return `np.array([])`
            `landmarks`: ndarray of shape (n_bboxes, n_landmarks, 2)
                The (x, y) landmarks of each face.
                The coordinates are floating point absolute coordinates w.r.t the image
                In case of no face detected, return `np.array([])`
        """
        raise NotImplementedError

class IFacePreprocessor:
    def __init__(self):
        pass

    def process(selfimage, bboxes, landmarks):
        """Preprocess face bounding boxes within an image
        
        Arguments:
            `image`: a ndarray of shape (n_faces, h, w, c)
                Input BGR image
            `bboxes`: a ndarray of shape (n_bboxes, 5)
                Each face bbox in `image` is of the form (x1, y1, x2, y2, score).
                The coordinates are floating point absolute coordinates w.r.t the image.
            `landmarks`: a ndarray of shape (n_bboxes, n_landmarks, 2)
                The landmarks of each face in `image`.
                The coordinates are floating point absolute coordinates w.r.t the image.
        Returns:
            `cropped_faces`: a ndarray of shape (n_faces, h, w, c)
                Each cropped face is a BGR image containing a single face only
        Errors:
            1. When len(bboxes) = 0
        """
        raise NotImplementedError

class IFaceFilter:
    def __init__(self):
        pass

    def process(self, cropped_faces):
        """
        Arguments:
            `cropped_faces`: a ndarray of shape (n_faces, h, w, c)
                Each face is a BGR image containing a single face only
        Returns:
            `cropped_faces`: a ndarray of shape (n_faces, h, w, c)
                Each face is a BGR image containing a single face only
        Errors:
            1. When len(cropped_faces) = 0
        """
        raise NotImplementedError

class IFaceDescriptor:
    def __init__(self):
        pass

    def process(self, cropped_faces):
        """Extract face embeddings using InsightFace
        
        Arguments:
            `cropped_faces`: a ndarray of shape (n_samples, h, w, c)
                Each face is a BGR image containing a single face only
        Returns:
            `face_embeddings`: a ndarray of shape (n_samples, n_features) 
                Embeddings of input faces
        Errors:
            1. When len(cropped_faces) = 0
        """
        raise NotImplementedError

class IFaceSearcher:
    def __init__(self):
        pass

    def predict(self):
        """        
        Arguments:
            `face_embeddings`: a ndarray of shape (n_samples, n_features) 
                Embeddings of input faces
        Returns:
            `preds`: list of `str`
                ID of input faces
        Errors:
            1. When len(face_embeddings) = 0
            2. face_embeddings.dtype != np.float32
        """
        raise NotImplementedError