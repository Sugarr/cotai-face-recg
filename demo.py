import os
import glob
import sys
import random
import time
from datetime import datetime
import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
from PIL import Image
from sklearn.neighbors import BallTree
from business_layer.core.insightface import (
    InsightFaceMTCNNDetector,
    InsightFacePreprocessor,
    InsightFaceDescriptor,
    KNNFaceSearcher
)
import natsort
import json

from business_layer.core.dlibfaces import DlibDetector
from business_layer.core.pipeline import (
    InputImageFilter,
    FaceDetector,
    InputFaceFilter,
    FacePreprocessor,
    FaceDescriptor,
    FaceSearcher
)
from config import default
import argparse

def ids_to_label(ids,dist, label):
    ls = []
    for i in range(len(ids)):
        predict = ids[i]
        distance = dist[i]
        if predict.size == 0:
            ls.append(-1)
        else:
            
            score_class = np.zeros(max(label)+1)
            for j in range(len(predict)):
                score_class[label[predict[j]]] += 1/distance[j]
            ls.append(np.argmax(score_class))
    return ls


def resize(image, newsize):
    """Resize `image` to `newsize` while keeping proportions
    
    Arguments:
        `image`: ndarray of shape (h, w, c)
            BGR image
        `newsize`: tuple of 2 ints
            Width and height
    Returns:
        `resized_image`: ndarray of shape (nh, nw, c)
            Resized BGR image
    """
    maxscale = max(newsize[0]/image.shape[1], newsize[1]/image.shape[0])
    padded_image_size = (int(maxscale/newsize[0]*image.shape[1]*image.shape[1]), 
                         int(maxscale/newsize[1]*image.shape[0]*image.shape[0]))
    padtop = max((padded_image_size[1] - image.shape[0])//2, 0)
    padbottom = max((padded_image_size[1] - image.shape[0]) - padtop, 0)
    padleft = max((padded_image_size[0] - image.shape[1])//2, 0)
    padright = max((padded_image_size[0] - image.shape[1]) - padleft, 0)
    image = np.pad(image, ((padleft, padright), (padtop, padbottom), (0, 0)), mode='constant')
    image = cv2.resize(image, newsize)
    return image

def visualize_detection_results(image, bboxes, landmarks, face_validities):
    for i, (bbox, landmark) in enumerate(zip(bboxes, landmarks)):
        bbox = bbox.astype(int)
        landmark = landmark.astype(int)
        
        if face_validities[i]:
            color = (0, 255, 0)
        else:
            color = (0, 0, 255)
        
        cv2.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]), color, 5)
        for point in landmark:
            cv2.circle(image, (point[0], point[1]), 3, color, 2)
    return image

print("Loading Embedding and Tree ...")
embedding = np.load('./emb_and_tree/x_train_5.npy')
label = np.load('./emb_and_tree/y_label_train_5.npy')
tree = BallTree(embedding)


print("Loading config from config.py ...")


input_image_filter = InputImageFilter(default.MIN_IMAGE_WIDTH,
                                      default.MIN_LUX_ILLUMINATION,
                                      default.MIN_NUMBER_OF_REGISTERED_IMAGE_PER_FACE)


detector = DlibDetector(default.DLIB_DETECTOR_MODEL, 
                        default.DLIB_LANDMARKS_MODEL, 
                        default.DLIB_INPUT_SCALE,
                        default.DLIB_FACE_DETECTOR_GPU_IDX)
detector = FaceDetector(detector)

input_face_filter = InputFaceFilter(default.MIN_RELATIVE_PIXEL_DISTANCE_BETWEEN_EYES,
                                    default.MIN_REGISTRATION_PIXEL_DISTANCE_BETWEEN_EYES,
                                    default.MIN_DETECTION_PIXEL_DISTANCE_BETWEEN_EYES,
                                    default.MIN_RECOGNITION_PIXEL_DISTANCE_BETWEEN_EYES)

preprocessor = InsightFacePreprocessor(default.INSIGHTFACE_INPUT_IMAGESIZE)
preprocessor = FacePreprocessor(preprocessor)

descriptor = InsightFaceDescriptor(default.INSIGHTFACE_DESCRIPTOR_MODEL_DIR,
                                   None,
                                   default.INSIGHTFACE_INPUT_IMAGESIZE)

descriptor = FaceDescriptor(descriptor)

# searcher = KNNFaceSearcher(default.FAISS_KNN_INDEX_FILE,
#                            default.FAISS_KNN_IDX_TO_ID_FILE,
#                            default.FAISS_KNN_ID_TO_IDX_FILE,
#                            default.FAISS_KNN_METADATA_FILE,
#                            default.INSIGHTFACE_DESCRIPTOR_OUTPUT_SIZE,
#                            default.FAISS_KNN_K,
#                            default.FAISS_KNN_THRESHOLD,
#                            None)
# searcher = FaceSearcher(searcher)


def capture_and_predict(path_data_history,ids_to_name):
    cap = cv2.VideoCapture(0)
    runtime = {"readtime": [],
            "preprocesstime": [],
            "inputfiltertime": [], 
            "detecttime": [],
            "facefiltertime": [],
            "facepreprocesstime": [],
            "describetime": []}
    count = 0
    cnt = 0
    dist_history = {}
    name_st = "unknown"
    while True:        
        start = time.time()
        ret, frame = cap.read()
        frame_raw = frame.copy()
        runtime["readtime"].append(time.time() - start)
        if ret == False:
            # print(true_label, video_path)
            print(ret)
            break
        start = time.time()
        # Resize image và tạo list chứa dictionary
        frame = resize(frame, (1280, 720))
        items = [{"image": frame, "task": "detection"}]
        runtime["preprocesstime"].append(time.time() - start)
        
        # Kiểm tra input có phù hợp yêu cầu không (Yêu cầu có để cập trong file insigtface.py)
        start = time.time()
        items = input_image_filter.filter(items)
        runtime["inputfiltertime"].append(time.time() - start)
        # Chỉ tiếp tục cắt khuôn mặt khi tấm hình phù hợp yêu cầu

        # Nhận diện khuông mặt trong tấm hình input
        start = time.time()
        items = detector.detect(items)
        runtime["detecttime"].append(time.time() - start)
        
        if items[0]["validity"] == True:
            if cnt % 4 == 0:
                cv2.imwrite("data_history/" + str(cnt/4) + ".jpg", frame)
            cnt += 1

            # Kiểm tra bboxes và landmarks
            start = time.time()
            items = input_face_filter.filter(items)
            runtime["facefiltertime"].append(time.time() - start)

            # Xử lý khuôn mặt
            start = time.time()
            items = preprocessor.preprocess(items)
            runtime["facepreprocesstime"].append(time.time() - start)

            # Lấy embedding của khuôn mặt sử dụng model Arcface 
            start = time.time()
            items = descriptor.describe(items)
            runtime["describetime"].append(time.time() - start)
            start = time.time()

            # Sử dụng embedding để dự đoán tên
            ids, dist = tree.query_radius(items[0]['embeddings'],  r = 0.6, return_distance = True)

            y_predict = ids_to_label(np.array(ids), np.array(dist), label)


            print("\nids",ids)
            print("\ndist",dist)

            if y_predict[0] != -1:
                name_st = ids_to_name[str(y_predict[0])]
                cv2.putText(frame,  name_st, (20, 80), cv2.FONT_HERSHEY_SIMPLEX, 1, (30, 255, 30), 2, cv2.LINE_AA)
                print(y_predict[0])
                if not os.path.exists(path_data_history + "/" + str(y_predict[0])):
                    print(path_data_history + "/" + str(y_predict[0]))
                    os.mkdir(path_data_history + "/" + str(y_predict[0]))
                cv2.imwrite(path_data_history + "/" + str(y_predict[0]) + "/" + str(count) + ".jpg", frame)
                
                info_person_i_in_box = save_dist(ids_to_name, ids[0], dist[0], label)
                print(path_data_history + "/" + str(y_predict[0]) + "/" + str(count) + ".jpg")
                dist_history[str(y_predict[0]) + "/" + str(count) + ".jpg"] = info_person_i_in_box
                count += 1
            else:
                cv2.putText(frame,  name_st, (20, 80), cv2.FONT_HERSHEY_SIMPLEX, 1, (30, 30, 255), 2, cv2.LINE_AA)
            
            frame = visualize_detection_results(items[0]["image"], items[0]["bboxes"], items[0]["landmarks"],
                                    items[0]["face_validities"])
        else:
            name_st = "unknown"
        cv2.putText(frame, str(time.time() - start), (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 1, (30, 255, 30), 2, cv2.LINE_AA)

        cv2.imshow('Frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('s'):
            break
    return dist_history

# HUNG EDIT
# output: my_dict={"Long":[0.3,0.3,0.31], "Hung":[0.2,0.21]}
def save_dist(list_name, ids_i, dist_i, label):
    label_list = []
    # print('__________id',id)
    if ids_i.size == 0:
        if len(label_list)!=0:
            label_list.append(-1)
        else:
            label_list = [-1]
    else:
        label_list=label[ids_i]
    # print('___label_list', label_list)
    my_dict = {}
    for j in range(len(dist_i)):
        if list_name[str(label_list[j])] in my_dict.keys():
            my_dict[list_name[str(label_list[j])]].append(dist_i[j])
        else:
            my_dict[list_name[str(label_list[j])]] = [dist_i[j]]
    return my_dict

if __name__ == '__main__':

    ids_to_name = json.load(open("index_to_name_test.json"))
    # list_name = []
    # for id, name in ids_to_name.items():
    #     list_name.append(name)
    print(type(ids_to_name))
    now = datetime.now()
    path_data_history = str(now.day)+'_'+str(now.month)+'_'+str(now.year)+'_'+str(now.hour)+'_'+str(now.minute)+'_'+str(now.second)

    if not os.path.exists(path_data_history):
        print(os.path.exists(path_data_history))
        os.makedirs(path_data_history)
    dist_history =  capture_and_predict(path_data_history,ids_to_name)
    file_name = os.path.join(path_data_history,'debug.json') 
    with open(file_name, 'w') as debug:
        json.dump(dist_history, debug, ensure_ascii=False, indent=4)


    